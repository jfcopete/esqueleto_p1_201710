package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.ListaEncadenada;
import model.vo.VOTag;

public class view_user {

private final static String Ruta_TAGS="./data/tags.csv";
	
	
	private static void printMenu(){
		System.out.println("1. Cargar Tags");
		System.out.println("2. Para Ver Los Tags");
		//System.out.println("3. Para saber solo el nombre de las peliculas");
		//System.out.println("4. Para obtener una lista de a�os");

	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printMenu();
		Scanner sc = new Scanner(System.in);
		int option = sc.nextInt();
		switch (option) {
		case 1:
			Controller.CargarTags(Ruta_TAGS);
			break;
		case 2:
			ListaEncadenada<VOTag> tags = Controller.darTags();
			for (VOTag tag : tags) {
				System.out.println(tag.getTag());
			}
			break;
		default:
			break;
		}
	}

}
