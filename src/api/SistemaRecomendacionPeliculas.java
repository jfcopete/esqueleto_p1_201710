package api;

import java.io.BufferedReader;
import java.io.FileReader;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.Stack;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioGenero;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {


	private VOPelicula VoPelicula = new VOPelicula();

	private ListaEncadenada<VOTag> tags = new ListaEncadenada<>();

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(rutaTags));
			String line = br.readLine();
			while (line!=null) {
				VOTag VoTag = new VOTag();
				String partsOfTags [] = line.split(",");
				//int UserID = Integer.parseInt(partsOfTags[0]);
				//int PeliculaID = Integer.parseInt(partsOfTags[1]);
				String tag = partsOfTags[2];
				long timestamp = Long.parseLong(partsOfTags[3]);
				VoTag.setTag(tag);
				VoTag.setTimestamp(timestamp);
				tags.agregarElementoFinal(VoTag);
			}
			if (tags.size()!=0) {
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return false;
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tags.size();
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		// TODO Auto-generated method stub

	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub

	}
	
	
	
	
	
	
	////////////////////////////////////////////////////////////
	/*
	 * Metodos de PRUEBA
	 */
	///////////////////////////////////////////////////////////
	public ListaEncadenada<VOTag> darTags() {
		return tags;
	}

}
